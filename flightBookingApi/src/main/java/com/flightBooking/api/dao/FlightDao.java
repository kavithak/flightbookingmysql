package com.flightBooking.api.dao;

import org.springframework.data.repository.CrudRepository;

import com.flightBooking.api.model.Flight;

/**
 * @author Kavitha
 * Flight Dao interface
 */
public interface FlightDao extends CrudRepository<Flight, Integer> {

}
