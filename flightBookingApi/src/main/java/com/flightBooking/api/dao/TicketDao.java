package com.flightBooking.api.dao;

import org.springframework.data.repository.CrudRepository;

import com.flightBooking.api.model.Ticket;

/**
 * @author Kavitha
 * Ticket Dao interface
 */
public interface TicketDao extends CrudRepository<Ticket, String> {

}
