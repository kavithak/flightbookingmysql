package com.flightBooking.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Kavitha
 * Spring boot starter class
 */
@SpringBootApplication
public class FlightBookingApp {

	public static void main(String[] args) {
		SpringApplication.run(FlightBookingApp.class, args);
	}
}
