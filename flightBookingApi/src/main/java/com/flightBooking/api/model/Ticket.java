package com.flightBooking.api.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Kavitha Ticket entity class
 */
@Entity
@Table(name = "Ticket")
public class Ticket {

	private int economySaver;

	private int economyFlex;

	private int bussinesClass;

	private int firstClass;

	public Ticket(int economySaver, int economyFlex, int bussinesClass, int firstClass) {
		super();
		this.economySaver = economySaver;
		this.economyFlex = economyFlex;
		this.bussinesClass = bussinesClass;
		this.firstClass = firstClass;
	}

	public int getEconomySaver() {
		return economySaver;
	}

	public void setEconomySaver(int economySaver) {
		this.economySaver = economySaver;
	}

	public int getEconomyFlex() {
		return economyFlex;
	}

	public void setEconomyFlex(int economyFlex) {
		this.economyFlex = economyFlex;
	}

	public int getBussinesClass() {
		return bussinesClass;
	}

	public void setBussinesClass(int bussinesClass) {
		this.bussinesClass = bussinesClass;
	}

	public int getFirstClass() {
		return firstClass;
	}

	public void setFirstClass(int firstClass) {
		this.firstClass = firstClass;
	}

}
