package com.flightBooking.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Kavitha
 * Ticket Entity class
 */
@Entity
@Table(name = "Flight")
public class Flight {
	
	@Id
	@GeneratedValue
	private int id;
	
	@Column(length=50, nullable=false )
	private String flightId;
	
	@Column(length=100, nullable=false )
	private  String sourceCode;
	
	@Column(length=100, nullable=false )
	private String destinationCode;

	public Flight(int id, String flightId, String sourceCode, String destinationCode) {
		super();
		this.id = id;
		this.flightId = flightId;
		this.sourceCode = sourceCode;
		this.destinationCode = destinationCode;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFlightId() {
		return flightId;
	}

	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}

	public String getSourceCode() {
		return sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public String getDestinationCode() {
		return destinationCode;
	}

	public void setDestinationCode(String destinationCode) {
		this.destinationCode = destinationCode;
	}
	
	
	

}
